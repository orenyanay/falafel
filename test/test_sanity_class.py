import pytest
import unittest
import DB
# from DB.db_methods import DBMethods
from DB import db_methods


class FALAFELTests(unittest.TestCase):
    dbm = db_methods.DBMethods()

    def test_sanity(self):

        actual_data, row_id = self.dbm.generate_data_from_table()
        expected_data = \
            dict(siteId='nastysite-dev.s3-website-us-east-1.amazonaws.com', depthLevel=2, directorySize=9432604,
                 numberOfPages=9, numberOfAnalyzedPages=0, numberOfFailedPages=0, status='done',
                 downloadPath='downloads/qa/nastysite-dev.s3-website-us-east-1.amazonaws.com/{0}'.format(row_id),
                 logFile='downloads/qa/nastysite-dev.s3-website-us-east-1.amazonaws.com/{0}/log_file.txt'.format(
                     row_id), downloadReturnValue=8,
                 errorMessage='Server issued an error response: returned non-zero exit status', excludeFiles='zip',
                 includeFiles='html,htm,mp4,avi,ogg,gif,png,jpeg,webp,jpg',
                 url='nastysite-dev.s3-website-us-east-1.amazonaws.com', result='porn', resultDescription='porn_1',
                 aiResult='porn',
                 aiResultDescription='{"clean": 0.2728424609320006, "escort": 0.0, "porn": 0.7271575390679994}',
                 moderatedId=None, redirectedCode=None, siteCode=200,
                 spiderResponse="out:[0] Checking 'http://nastysite-dev.s3-website-us-east-1.amazonaws.com' ...\nHTTP response 200 OK [http://nastysite-dev.s3-website-us-east-1.amazonaws.com]\n\nerr:",
                 emptyImages=0, emptyVideos=0, redirectedURL=None, modelName='scanner_sites_2020_04_16_7_38',
                 collectionName='sanity')
        ret = False
        if actual_data is not None:
            ret = self.dbm.compare_expected_data_with_actual_data(expected_data, actual_data)
            print(ret)
            self.assertTrue(ret), "test Passed :)"
        else:
            self.assertFalse(ret), "test FAILED :("


if __name__ == '__main__':
    unittest.main()

# t = FALAFEL_old()
# t.test_sanity()
