import pickle
import time

from DB.db_connector import DBConnector


class DBMethods:
    def __init__(self):
        self.db_connector = DBConnector()

    def get_data_from_table(self, query="select * from jobs;"):
        if self.db_connector is not None:
            cursor = self.db_connector.connection.cursor()
            cursor.execute(query)
            records = cursor.fetchall()
            # cursor.description[0][0] + ' ' + str(records[0][0]) - 'id 1' - column name , column value
            header = cursor.description
            # d = pickle.dump(records)
            # with open('data.pickle', 'wb') as f:
            #     d = pickle.dump(records, f, pickle.HIGHEST_PROTOCOL)
            # with open('data.pickle', 'rb') as f:
            #     data = pickle.load(f)
            # print(cursor.description)
            cursor.close()
            self.db_connector.close_connection_to_db()
            return header, records

    def generate_data_from_table(self, query="select * from jobs;"):
        # getting data from db
        header, records = self.get_data_from_table(query)
        header_list = []
        record_list = []
        data_dict = {}
        # generating db table column names
        for item in header:
            header_list.append(item[0])

        # working on record that has status done
        record = records[0]
        iter_no = 0
        while 'done' not in record:
            if iter_no < 4:
                print('job is still running')
                iter_no += 1
                print('sleeping for 5 seconds, iter ', iter_no)
                time.sleep(5)
                self.db_connector.open_connection_to_db(self.db_connector.db_name)
                header, records = self.get_data_from_table(query)
                record = records[0]
            else:
                print("reached retry limit...")
                return None

        print('job has finished running.')

        # for record in records:
        #     if 'done' in record:
        #         print('job has finished running.')
        #         record_list.append(record)
        #         break

        for header_item, record_item in zip(header_list, list(record)):
            data_dict[header_item] = record_item

        print('here')

        return data_dict, data_dict['id']

    def compare_expected_data_with_actual_data(self, expected_data_dict, actual_data_dict):
        for k, v in zip(expected_data_dict, actual_data_dict):
            if expected_data_dict[k] == actual_data_dict[k]:
                print('found match for ', expected_data_dict[k])
            else:
                print(expected_data_dict[k])
                print('found mismatch, test failed :(')
                return False

        return True


# dbm = DBMethods()
# actual_data = dbm.generate_data_from_table()
# expected_data = {'siteId': 'nastysite-dev.s3-website-us-east-1.amazonaws.com', 'depthLevel': 2,
#                  'directorySize': 9432604, 'numberOfPages': 9,
#                  'numberOfAnalyzedPages': 0, 'numberOfFailedPages': 0, 'status': 'done',
#                  'downloadPath': 'downloads/qa/nastysite-dev.s3-website-us-east-1.amazonaws.com/9',
#                  'logFile': 'downloads/qa/nastysite-dev.s3-website-us-east-1.amazonaws.com/9/log_file.txt',
#                  'downloadReturnValue': 8,
#                  'errorMessage': 'Server issued an error response: returned non-zero exit status',
#                  'excludeFiles': 'zip', 'includeFiles': 'html,htm,mp4,avi,ogg,gif,png,jpeg,webp,jpg',
#                  'url': 'nastysite-dev.s3-website-us-east-1.amazonaws.com', 'result': 'porn',
#                  'resultDescription': 'porn_1', 'aiResult': 'porn',
#                  'aiResultDescription': '{"clean": 0.2728424609320006, "escort": 0.0, "porn": 0.7271575390679994}',
#                  'moderatedId': None, 'redirectedCode': None, 'siteCode': 200,
#                  'spiderResponse': "out:[0] Checking 'http://nastysite-dev.s3-website-us-east-1.amazonaws.com' ...\nHTTP response 200 OK [http://nastysite-dev.s3-website-us-east-1.amazonaws.com]\n\nerr:",
#                  'emptyImages': 0, 'emptyVideos': 0,
#                  'redirectedURL': None, 'modelName': 'scanner_sites_2020_04_16_7_38', 'collectionName': 'sanity'}
# ret = dbm.compare_expected_data_with_actual_data(expected_data, actual_data)
# print(ret)

# print('\n'.join(map(str, r)))
