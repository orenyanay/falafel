import mysql.connector
from mysql.connector import Error


class DBConnector:
    def __init__(self, db_name='scannerqa'):
        self.connection = None
        self.db_name = db_name
        self.open_connection_to_db(db_name)

    def open_connection_to_db(self, db_name):
        try:
            self.connection = mysql.connector.connect(host='172.16.1.2', database=self.db_name,
                                                      user='admin', password='Antitoxin613')
            if self.connection.is_connected():
                print("connected to db successfully...")
                return self.connection
        except Error as e:
            print("SQL error: ", e)

    def close_connection_to_db(self):
        if self.connection.is_connected():
            print("closing connection to db...")
            self.connection.close()
            print("connection to db closed.")


# db = DBConnector()
# db.open_connection_to_db()
