import requests


class SpiderAPI:

    def __init__(self, domain):
        self.domain = domain
        self.get_domain(domain)

    def get_domain(self, domain):
        if domain.startswith("http://") is not True or domain.startswith("www.") is not True:
            print("changing url with added http to domain...")
            self.domain = "http://" + domain
        else:
            self.domain = domain

    def run_get(self, param_list=None):
        tries = 0
        try:
            response = requests.get(self.domain, params=param_list)
        except Exception as e:
            print("ERROR: ", e)
            tries += 1
            if tries == 1:
                response = requests.get(self.domain, params=param_list)
            else:
                return None

        print(response)
        return response

    def run_post(self, data_list=None):
        tries = 0
        try:
            post = requests.post(self.domain, data=data_list)
        except Exception as e:
            tries += 1
            if tries == 1:
                post = requests.post(self.domain, data={'key': 'value'})
            else:
                return None

        print(post)
        return post
